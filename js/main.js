document.addEventListener('DOMContentLoaded', () => {

    let LangBtn = document.querySelector('.lang-current')
    let LangWrapper = document.querySelector('.action-lang')
    window.ScrollWidth = document.querySelector('.header-menu').scrollWidth;
    window.MenuWidth = 0
    window.ScreenWidth = document.body.offsetWidth;
    window.MenuInit = false
    CalcWidthLink()
    ShowMoreMenu()
    setInterval(function (){
        if (window.ScrollWidth === 0){
            window.ScrollWidth = document.querySelector('.header-menu').scrollWidth;
        }
        if (window.MenuWidth === 0){
            CalcWidthLink()
        }
        if (window.ScreenWidth === 0){
            window.ScreenWidth = document.body.offsetWidth;
        }
        if (!window.MenuInit){
            ShowMoreMenu();
        }
    }, 1000)

    LangBtn.addEventListener('click', () => {

        if (LangWrapper.classList.contains('lang-show')){
            LangWrapper.classList.remove('lang-show')
        } else {
            LangWrapper.classList.add('lang-show')
        }

    })


    window.addEventListener('resize', ShowMoreMenu)


    function ShowMoreMenu() {
        let MenuBlock = document.querySelector('.header-menu')
        let ScrollWidth = MenuBlock.scrollWidth
        let MenuDropdown = document.querySelector('.menu-dropdown');
        let Links = document.querySelectorAll('.hidden-link')

        let WrapperWidth = document.querySelector('.header-wrapper').offsetWidth
        let LogoWidth = document.querySelector('.header-logo').offsetWidth
        let ActionWidth = document.querySelector('.header-actions').offsetWidth

        let BlockWidth = WrapperWidth - LogoWidth - ActionWidth

        if (!window.MenuInit && BlockWidth < MenuBlock.clientWidth){
            Links.forEach((item) => {

                if (BlockWidth <= parseInt(item.getAttribute('max-content')) ){
                    console.log('hidden')
                    item.style.display = 'none'
                    window.ScreenWidth = document.body.offsetWidth
                    item.classList.add('dis-link')
                    $('.drop-link[data-link="'+item.dataset.link+'"]').css({
                        display: 'block'
                    })
                }
                if ($('.hidden-link').hasClass('dis-link')){
                    MenuDropdown.classList.add('menu-dropdown-show')
                } else {
                    MenuDropdown.classList.remove('menu-dropdown-show')
                }
            })
            window.MenuInit = true
        }

        if (window.ScreenWidth > document.body.offsetWidth){
            Links.forEach((item) => {

                if (BlockWidth <= parseInt(item.getAttribute('max-content')) ){
                    console.log('hidden')
                    item.style.display = 'none'
                    window.ScreenWidth = document.body.offsetWidth
                    item.classList.add('dis-link')
                    $('.drop-link[data-link="'+item.dataset.link+'"]').css({
                        display: 'block'
                    })
                }
                if ($('.hidden-link').hasClass('dis-link')){
                    MenuDropdown.classList.add('menu-dropdown-show')
                } else {
                    MenuDropdown.classList.remove('menu-dropdown-show')
                }
            })
        }
        if (window.ScreenWidth < document.body.offsetWidth){
            Links.forEach((item) => {

                if (BlockWidth >= parseInt(item.getAttribute('max-content'))){
                    console.log('show')
                    item.style.display = 'block'
                    window.ScreenWidth = document.body.offsetWidth
                    item.classList.remove('dis-link')
                    $('.drop-link[data-link="'+item.dataset.link+'"]').css({
                        display: 'none'
                    })
                }
                if ($('.hidden-link').hasClass('dis-link')){
                    MenuDropdown.classList.add('menu-dropdown-show')
                } else {
                    MenuDropdown.classList.remove('menu-dropdown-show')
                }

            })
        }



    }


    function CalcWidthLink(){


        let WrapperWidth = document.querySelector('.header-wrapper').offsetWidth
        let LogoWidth = document.querySelector('.header-logo').offsetWidth
        let ActionWidth = document.querySelector('.header-actions').offsetWidth
        let Links = document.querySelectorAll('.hidden-link')

        let Width = 0

        window.MenuWidth = WrapperWidth - LogoWidth - ActionWidth


        Links.forEach((item) => {
            let width = item.offsetWidth
            Width = Width + width
            item.setAttribute('min-content', Width);
            item.setAttribute('max-content', Width + 5);
        })




    }

})